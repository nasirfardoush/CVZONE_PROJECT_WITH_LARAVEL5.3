<article class="hs-content service-section" id="section4">
    <span class="sec-icon serv-icon fa fa-laptop"></span>
    <div class="hs-inner">
        <span class="before-title">.04</span>
        <h2>SERVICES</h2>
        <span class="content-title">OUR SERVICES</span>
        <div class="services-wrap">

        @foreach ($allData['services'] as $servicesData)

            <div class="serv-wrap animated slideInDown">
                <!-- <span class="fa serv-icons fa-crop"></span> -->
                <span > <img class="serviceimage" src="{{ asset('/images').'/'.$servicesData->img }}"> </span>
                <h3>{{ $servicesData->title }}</h3>
                <p>{{ $servicesData->description }}</p>
                <div class="slide">
                    <h4>Services</h4>
                    <ul>
                        @php
                                $topicsdata = explode(',',$servicesData->topics);
                         @endphp
                        @foreach ($topicsdata as $topics)
                                <li><i class="fa fa-check"></i> {{ $topics }}</li>
                         @endforeach
                    </ul>
                </div>
            </div>
       @endforeach
        </div>
        <span class="content-title">WHAT OUR CLIENTS SAYING</span>
        <div class="testimonials-container">
          @foreach ($allData['services'] as $servicesData)
            <div class="testimonial">
                <figure class="testimonial__mug">
                    <img src="{{ asset('/images/').'/'.$servicesData->client_image }}" height="100px" width="100px">
                </figure>
                <p>&ldquo;{{ $servicesData->clinte_feedback }}&rdquo;
                    <br><!-- <strong>John, Taunton</strong> -->
                </p>
            </div>
        @endforeach
            <button class="prev-testimonial">Prev</button>
            <button class="next-testimonial">Next</button>
        </div>
</article>