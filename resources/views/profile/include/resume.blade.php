<article class="hs-content resume-section" id="section2">
    <span class="sec-icon fa fa-newspaper-o"></span>
    <div class="hs-inner">
        <span class="before-title">.02</span>
        <h2>RESUME</h2>
        <!-- Resume Wrapper -->
        <div class="resume-wrapper">
            <ul class="resume">
                <!-- Resume timeline -->
                <li class="time-label">
                    <span class="content-title">EDUCATION</span>
                </li>
                @foreach ($allData['educations'] as $eduData)
                      
                <li>
                    <div class="resume-tag">
                        <span class="fa fa-graduation-cap"></span>
                        <div class="resume-date">
                            <span>{{ $eduData->enrolled_year }}</span>
                            <div class="separator"></div>
                            <span>{{ $eduData->passing_year }}</span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <span class="timeline-location"><i class="fa fa-map-marker"></i>{{ $eduData->location }}</span>
                        <h3 class="timeline-header">{{ $eduData->title }}- {{ $eduData->degree }}</h3>
                        <div class="timeline-body">
                            <h4>{{ $eduData->institute }}</h4>
                            <span>
                                <p>{{ $eduData->description }}</p>
                                <label>Result:</label>{{ $eduData->result }}
                                <label> Board:</label>{{ $eduData->education_board }}
                                <label> Durations:</label>{{ $eduData->course_duration }}
                            </span>
                        </div>
                    </div>
                </li>
                @endforeach   
                <li class="time-label">
                    <span class="content-title">ACADEMIC AND PROFESSIONAL POSITIONS</span>
                </li>

                @foreach ($allData['experiences'] as $experData)
                <li>
                    <div class="resume-tag">
                        <span class="fa fa-university"></span>
                        <div class="resume-date">
                            <span>{{ $experData->start_date }}</span>
                            <div class="separator"></div>
                            <span>{{ $experData->end_date }}</span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <span class="timeline-location"><i class="fa fa-map-marker"></i>{{ $experData->company_location }}</span>
                        <h3 class="timeline-header">{{ $experData->designation }}</h3>
                        <div class="timeline-body">
                            <h4>{{ $experData->company_name }}</h4>
                            <span>{{ $experData->expreince_desc }}</span>
                        </div>
                    </div>
                 </li>   
                @endforeach
                <li class="time-label">
                    <span class="content-title">HONORS AND AWARDS</span>
                </li>
                @foreach ($allData['awards'] as $awardsData)
                <li>
                    <div class="resume-tag">
                        <span class="fa fa-star-o"></span>
                        <div class="resume-date">
                            <span>{{ $awardsData->year }}</span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <span class="timeline-location"><i class="fa fa-map-marker"></i>{{ $awardsData->location }}</span>
                        <h3 class="timeline-header">{{ $awardsData->title }}</h3>
                        <div class="timeline-body">
                            <h4>{{ $awardsData->organization }}</h4>
                            <span>{{ $awardsData->description }}</span>
                        </div>
                    </div>
                </li>
                @endforeach
                <!-- End Resume timeline -->
            </ul>
        </div>
        <!-- End Resume Wrapper -->
    </div>
</article>