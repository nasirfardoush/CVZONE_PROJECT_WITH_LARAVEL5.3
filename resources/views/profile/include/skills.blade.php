<article class="hs-content skills-section" id="section6">
    <span class="sec-icon fa fa-diamond"></span>
    <div class="hs-inner">
        <span class="before-title">.06</span>
        <h2>SKILLS</h2>
        @foreach ($allData['skills'] as $skillsData)

            <span class="content-title">{{ $skillsData->title }}</span>
            <div class="skolls">
                <span class="skill-description">{{ $skillsData->description }}</span>
                <div class="bar-main-container">
                    <div class="wrap">
                        <div class="bar-percentage" data-percentage="
                        @if ($skillsData['level']     == 'Intermediate') { echo "50";}
                        @elseif ($skillsData['level'] == 'Experts') {{ "60"}}
                        @elseif ($skillsData['level'] == 'Advance') {{ "80"}}
                        @elseif ($skillsData['level'] == 'Master') {{ "90" }}
                        @else {{"40"}}
                        @endif

                        "></div>
                        <span class="skill-detail"><i class="fa fa-bar-chart"></i>LEVEL : {{ $skillsData->level }}</span><span class="skill-detail"><i class="fa fa-binoculars"></i>EXPERIENCE : {{ $skillsData->experience }} YEARS</span>
                        <div class="bar-container">
                            <div class="bar"></div>
                        </div>

                        @php
                            $skillsField = explode(',',$skillsData->experience_area);
                        @endphp
                            @foreach ($skillsField  as $fields)
                            <span class="label">{{ $fields }}</span>
                         @endforeach
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>

     @endforeach
    </div>
</article>