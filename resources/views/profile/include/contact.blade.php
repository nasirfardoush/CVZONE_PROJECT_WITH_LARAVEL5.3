
<article class="hs-content contact-section" id="section8">
    <span class="sec-icon fa fa-paper-plane"></span>
    <div class="hs-inner">
        <span class="before-title">.08</span>
        <h2>CONTACT</h2>
        <div class="contact_info">
            <h3>Get in touch</h3>
            <hr>
            <h5>We are waiting to assist you</h5>
            <h6>Simply use the form below to get in touch</h6>
            @if(Session::has('message'))
                <div class="alert alert-info" >
                     {{ Session::get('message') }}
                 </div>

            @endif
            <hr>
        </div>
        <!-- Contact Form -->
        {!! Form::open(['url'=>'/contacts','method'=>'POST']) !!}
            <fieldset id="contact_form">
                <div id="result"></div>
                {!! Form::text('name',null,['placeholder'=>'NAME','id'=>'name']) !!}
                {!! Form::email('email',null,['placeholder'=>'EMAIL','id'=>'email']) !!}
                {!! Form::textarea('message',null,['placeholder'=>'MESSAGE','id'=>'message']) !!}

                {!! Form::submit('SEND MESSAGE',['class'=>'submit_btn']) !!}
                
            </fieldset>
      {!! Form::close() !!}

        <!-- End Contact Form -->
    </div>
</article>