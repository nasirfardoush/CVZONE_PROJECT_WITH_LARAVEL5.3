<article class="hs-content publication-section" id="section3">
<span class="sec-icon fa fa-pencil"></span>
<div class="hs-inner">
    <span class="before-title">.03</span>
    <h2>PUBLICATIONS</h2>
    <!-- Filter/Sort Menu -->
    <span class="content-title">PUBLICATIONS LIST</span>
    <div class="row publication-form">
        <div class="col-md-6 publication-filter">
            <div class="card-drop">
                <a class='toggle'>
                    <i class='icon-suitcase'></i>
                    <span class='label-active'>ALL</span>
                </a>
                <ul id="filter">
                    <li class='active'><a data-label="ALL" data-group="all">ALL</a>
                    </li>
                    @foreach ($allData['posts'] as $postsData)  
                    <li>
                        <a data-label="{{ $postsData->categories }}" data-group="{{ $postsData->categories }}"
                            {{ $postsData->categories }}
                        </a>
                    </li> 
                  @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-6 publication-sort">
            <div class="sorting-button">
                <span>SORTING BY DATE</span>
                <button class="desc"><i class="fa fa-sort-numeric-desc"></i>
                </button>
                <button class="asc"><i class="fa fa-sort-numeric-asc"></i>
                </button>
            </div>
        </div>
    </div>
    <!-- End Filter/Sort Menu -->
    <!-- publication wrapper -->
    <div id="mygrid">
        <!-- publication item -->
     @php
        $link =0;
      @endphp   
     @foreach ($allData['posts'] as $postData)
         @php
            $link++; 
         @endphp  

        <div class="publication_item" data-groups='["all","{{ $postsData->categories }}"]' data-date-publication="2007-12-01">
            <div class="media">
                <a href=".publication-detail{{ $link }}" class="ex-link open_popup" data-effect="mfp-zoom-out"><i class="fa fa-plus-square-o"></i></a>
                <div class="date pull-left">
                    <span style="font-size:20px;" class="day ">{{ $postsData->created_at }}</span>
                   <!--  <span class="month">DEC</span>
                    <span class="year">2007</span> -->
                </div>
                <div class="media-body">
                    <h3>{{ $postsData->title }}</h3>
                    <h4>{{ $postsData->city_name }} - {{ $postsData->country_name }}</h4>
                    <span class="publication_description">{{ substr($postData->description, 0, 100)}}...</span> </div>
                <hr style="margin:8px auto">
                <span class="label label-primary">Conferences</span>
                <span class="label selected">Selected</span>
                <span class="publication_authors"><strong>{{ $postsData->author_name }}</strong>
            </div>
            <div class="mfp-hide mfp-with-anim publication-detail{{ $link }} publication-detail">
                <div class="image_work">
                    <img class="img-responsive" src="{{asset('/images').'/'. $postsData->img }}" alt="img" width="480" height="200">
                </div>
                <div class="project_content">
                    <h3 class="publication_title">{{ $postsData->title }}</h3>
                    <span class="publication_authors"><strong>{{ $postsData->author_name }}</strong>
                    <span class="label label-primary">Conferences</span>
                    <span class="label selected">Selected</span>
                    <p class="project_desc">{{ $postsData->description }}</p>
                </div>
                <a class="ext_link" href="#"><i class="fa fa-external-link"></i></a>
                <div style="clear:both"></div>
            </div>
        </div>

      @endforeach
        <!-- End publication item -->

     <!-- End Publication item -->
    </div>
    <!-- End Publication Wrapper -->
</div>
<div class="clear"></div>
</article>