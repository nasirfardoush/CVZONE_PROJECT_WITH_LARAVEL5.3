<article class="hs-content teaching-section" id="section5">
    <span class="sec-icon fa fa-book"></span>
    <div class="hs-inner">
        <span class="before-title">.05</span>
        <h2>TEACHING</h2>
        <div class="teaching-wrapper">
            <ul class="teaching">
                <li class="time-label">
                    <span class="content-title">CURRENT</span>
                </li>
              @foreach ($allData['teaching'] as $teacData)  
                @if($teacData->teaching_status== 'CURRENT')
                     <li>
                        <div class="teaching-tag">
                            <span class="fa fa-suitcase"></span>
                            <div class="teaching-date">
                                <span>NOW</span>
                                <div class="separator"></div>
                                <span>{{$teacData->start_date}}</span>
                            </div>
                        </div>
                        <div class="timeline-item">
                            <h3 class="timeline-header">{{$teacData->title }}</h3>
                            <div class="timeline-body">
                                <h4>{{$teacData->institute }}</h4>
                                <span>{{$teacData->teaching_desc }}</span>
                            </div>
                        </div>
                    </li>               
              @endif
              @endforeach

                <li class="time-label">
                    <span class="content-title">TEACHING HISTORY</span>
                </li>
            @foreach ($allData['teaching'] as $teacData)  
                @if($teacData['teaching_status'] == 'PREVIOUS')
                <li>
                    <div class="teaching-tag">
                        <span class="fa fa-suitcase"></span>
                        <div class="teaching-date">
                            <span>{{$teacData->start_date }}</span>
                            <div class="separator"></div>
                            <span>{{$teacData->end_date }}</span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <h3 class="timeline-header">{{$teacData->title }}</h3>
                        <div class="timeline-body">
                            <h4>{{$teacData->institute }}</h4>
                            <span>{{$teacData->teaching_desc }}</span>
                        </div>
                    </div>
                </li>
              @endif
              @endforeach

            </ul>
        </div>
    </div>
</article>