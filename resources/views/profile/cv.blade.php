
<?php

/*User identification*/
 $email   = $allData['user']->email;
 $settingsData = $allData['settings'];

 $name    = $settingsData->fullname;
 $address = $settingsData->address;
 $image   = $settingsData->featured_img;
   
 $imgpath = asset('images');
 $img = (!empty($image))?$image:'man.jpg';

 $image   =   "$imgpath"."/"."$img";

 $aboutsData = $allData['about'];
    $phone   = $aboutsData->phone;
    $myself  = $aboutsData->short_desc;

 $eduInfo  = "";
foreach ($allData['educations'] as $eduData) {

        $eduInfo .= "<tr>";
        $eduInfo .= "<td>".$eduData->title."</td>";
        $eduInfo .= "<td>".$eduData->degree."</td>";
        $eduInfo .= "<td>".$eduData->institute."</td>";   
        $eduInfo .= "<td>".$eduData->course_duration."</td>";
        $eduInfo .= "<td>".$eduData->enrolled_year."-".$eduData->passing_year."</td>";
        $eduInfo .= "<td>".$eduData->result."</td>";     
        $eduInfo .= "<td>".$eduData->passing_year."</td>";
        $eduInfo .= "<td>".$eduData->education_board."</td>";
}


 $expreincesInfo  = ''; 
foreach ($allData['experiences'] as $expreincesData) {
    $expreincesInfo .= "<tr>"; 
    $expreincesInfo .= "<td colspan='2'>".$expreincesData->designation."</td>"; 
    $expreincesInfo .= "<td colspan='3'>".$expreincesData->company_name."</td>"; 
    $expreincesInfo .= "<td colspan='2'>".$expreincesData->company_location."</td>"; 
    $expreincesInfo .= "<td>".$expreincesData->start_date."-".$expreincesData->end_date."</td>";
    $expreincesInfo .= "</tr>";  

}

$awardsInfo  = '';
foreach ($allData['awards'] as $awardsData) {
    $awardsInfo .= "<tr>";
    $awardsInfo .= "<td colspan='2'>".$awardsData['title']."</td>";
    $awardsInfo .= "<td colspan='3'>".$awardsData['organization']."</td>";
    $awardsInfo .= "<td colspan='2'>".$awardsData['location']."</td>";
    $awardsInfo .= "<td>".$awardsData['year']."</td>";
    $awardsInfo .= "</tr>";
}

 $servicesInfo  = '';
foreach ($allData['services'] as $servicesData) {
    $servicesInfo .= "<tr>";
    $servicesInfo .= "<td colspan='2' >".$servicesData['title']."</td>";
    $servicesInfo .= "<td colspan='2'>".$servicesData['topics']."</td>";
    $servicesInfo .= "<td colspan='4'>".$servicesData['clinte_feedback']."</td>";
    $servicesInfo .= "</tr>";
}


$html=<<<EOD

<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>

</head>

<body>

    <div class="container">

        <!-- Image and address -->
        <div class="row cvWraper">
            <div class="">
                <table border="1" cellspacing="0">
                    <tr>
                        <td colspan="6">
                            <h3 class="cvTitle">CV  <span>of </span></h3>
                            <h4 class="cvTitle">$name</h4>
                            <h5>$address</h5>
                            <h5>$phone</h5>
                            <h5>$email</h5>
                        </td>
                        <td colspan="2">
                            <img width="150" height="130" src="$image">
                        </td>
                    </tr>
                    <!--Start Education Part -->
                    <tr>
                        <td colspan="8"><strong>Myself with few words:</strong>
                            <p class="text-justify">  $myself </p>
                        </td>
                    </tr>
                    <tr> <td colspan="8"><h5>Educations</h5></td></tr>
                    <tr>
                        <td>Title</td>
                        <td>Degree</td>
                        <td>Institute</td>
                        <td>Duration</td>
                        <td>Duration Years</td>
                        <td>Result</td>
                        <td>Passing Year</td>
                        <td>Board</td>
                    </tr>
                    $eduInfo;

                    <!--Start Education Part -->

                    <!-- Start  Experience Part -->
                    <tr>
                        <td colspan="8">
                            <h5>Experiences</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">TItle</td>
                        <td colspan="3">Company/Organization </td>
                        <td colspan="2">Location</td>
                        <td>Duration Year</td>
                    </tr>
                    $expreincesInfo;
                   
                    <!-- End Experience Part -->

                    <!-- Start  Awards Part -->
                    <tr>
                        <td colspan="8">
                            <h5>Awards</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">TItle</td>
                        <td colspan="3">Organization/Institute</td>
                        <td colspan="2">Location</td>
                        <td>Achieved Year</td>
                    </tr>
                    $awardsInfo;
                    <!-- End Awards Part -->


                    <!-- Start  Services Part -->
                    <tr>
                        <td colspan="8">
                            <h5>Services</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Title</td>
                        <td colspan="2">Topics</td>
                        <td colspan="4">Client Feedback</td>
                    </tr>
                    $servicesInfo;

                    <!-- End Services Part -->


                </table>





            </div>
        </div>

    </div>
</body>
</html
EOD;
$mpdf = new mPDF('s', array(210,297));
 $mpdf->mirrorMargins = 1;
 $mpdf->bleedMargin = 4;

  //Set left to right text
 $mpdf->SetDirectionality('ltr'); 
 $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
 

$mpdf->WriteHTML($html,2);
         
$mpdf->Output();

exit;

