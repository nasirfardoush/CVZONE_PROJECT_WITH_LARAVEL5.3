<!-- Include header -->
@include('profile.include.header')
 <!-- hs-content-wrapper -->
      <div class="hs-content-wrapper">
      	
         <!-- About section -->
            @include('profile.include.about')
             <!-- End About Section -->

                <!-- Resume Section -->
              	  @include('profile.include.resume')
                <!-- End Resume Section-->

                <!-- Publication Section -->
                   @include('profile.include.publication')
                <!-- End Publication Section -->

                <!-- service Section -->
                    @include('profile.include.service')
                <!-- End service Section -->

                <!-- Teaching Section -->
                    @include('profile.include.teaching')
                <!-- End Teaching Section -->

                <!-- Skills Section -->
                    @include('profile.include.skills')
                <!-- End Skills Section -->

                <!-- Works Section -->
                    @include('profile.include.works')
                <!-- End Works Section -->

                <!-- Contact Section -->
                    @include('profile.include.contact')
                <!-- End Contact Section -->
                </div>
                <!-- End hs-content-wrapper -->
            </div>
            <!-- End hs-content-scroller -->
        </div>
        <!-- End container -->
<!--  Include footer -->       
@include('profile.include.footer')     