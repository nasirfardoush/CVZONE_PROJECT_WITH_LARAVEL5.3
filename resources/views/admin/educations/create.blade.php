@extends('admin.layouts.master')
@section('educations_menu_add','active')
@section('pageTitle')
<span class="text-semibold">EDUCATIONS - ADD</span>  || <a href="/educations">MY EDUCATIONS</a>
@endsection

@section('content')
	<div class="row ">
		{!! Form::open(['url'=>'/educations','method'=>'POST']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Add your educations .</h5>
							@endif
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">
									
									{!! Form::label('title','Educations Title') !!}
									{!! Form::text('title',null,['placeholder'=>'Computer engineering','class'=>'form-control']) !!}
								</div>					
								<div class="form-group">
								    {!! Form::label('institute','Institute Name') !!}
									{!! Form::text('institute',null,['placeholder'=>'University of Dhaka','class'=>'form-control']) !!}
								</div>

								<div class="form-group">
								    {!! Form::label('enrolled_year','Enrolled Year') !!}
									{!! Form::text('enrolled_year',null,['placeholder'=>'2011','id'=>'datepicker','class'=>'form-control']) !!}
								</div>
								<div class="form-group">
								    {!! Form::label('result','Result') !!}
									{!! Form::text('result',null,['placeholder'=>'4.00','class'=>'form-control']) !!}
									<small>Enter result following GPA Standart</small>
								</div>									
								<div class="form-group">
								    {!! Form::label('education_board','Board') !!}
									{!! Form::text('education_board',null,['placeholder'=>'Technical','class'=>'form-control']) !!}
								</div>
							</div>								
							<!-- Second section -->							
							<div class="col-md-5">
								<div class="form-group">
							     	{!! Form::label('degree','Educations Degree') !!}
									{!! Form::text('degree',null,['placeholder'=>'MSC or BSc...','class'=>'form-control']) !!}
									<small>Please write sort name.</small>
								</div>					
								<div class="form-group">
								    {!! Form::label('location','Institute Location') !!}
									{!! Form::text('location',null,['placeholder'=>'Bangladesh','class'=>'form-control']) !!}
								</div>

								<div class="form-group">
								    {!! Form::label('passing_year','Passing Year') !!}
									{!! Form::text('passing_year',null,['placeholder'=>'2015','id'=>'datepicker2','class'=>'form-control']) !!}
									
								</div>									
								<div class="form-group">
							    	{!! Form::label('course_duration','Course duration(Years)') !!}
									{!! Form::text('course_duration',null,['placeholder'=>'4','class'=>'form-control']) !!}
								</div>																	
							</div>
						</div>
						<div class="form-group">
						{!! Form::submit('Save',['class'=>'marg-top']) !!}
							
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
   	</div>
@endsection