@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">EDUCATIONS - EDIT</span>  || <a href="/educations">MY EDUCATIONS</a> || <a href="/educations/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
		    <!-- Update your educations -->
		{!! Form::open(['url'=>['/educations',$education->id],'method'=>'PUT']) !!}
		<fieldset class="content-group">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						<h5>You can edit your educations.</h5>
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">
									
									{!! Form::label('title','Educations Title') !!}
									{!! Form::text('title', $education->title ,['placeholder'=>'Computer engineering','class'=>'form-control']) !!}
								</div>					
								<div class="form-group">
								    {!! Form::label('institute','Institute Name') !!}
									{!! Form::text('institute', $education->institute ,['placeholder'=>'University of Dhaka','class'=>'form-control']) !!}
								</div>

								<div class="form-group">
								    {!! Form::label('enrolled_year','Enrolled Year') !!}
									{!! Form::text('enrolled_year', $education->enrolled_year ,['placeholder'=>'2011','id'=>'datepicker','class'=>'form-control']) !!}
								</div>
								<div class="form-group">
								    {!! Form::label('result','Result') !!}
									{!! Form::text('result', $education->result ,['placeholder'=>'4.00','class'=>'form-control']) !!}
									<small>Enter result following GPA Standart</small>
								</div>									
								<div class="form-group">
								    {!! Form::label('education_board','Board') !!}
									{!! Form::text('education_board', $education->education_board ,['placeholder'=>'Technical','class'=>'form-control']) !!}
								</div>
							</div>								
							<!-- Second section -->							
							<div class="col-md-5">
								<div class="form-group">
							     	{!! Form::label('degree','Educations Degree') !!}
									{!! Form::text('degree', $education->degree ,['placeholder'=>'MSC or BSc...','class'=>'form-control']) !!}
									<small>Please write sort name.</small>
								</div>					
								<div class="form-group">
								    {!! Form::label('location','Institute Location') !!}
									{!! Form::text('location', $education->location ,['placeholder'=>'Bangladesh','class'=>'form-control']) !!}
								</div>

								<div class="form-group">
								    {!! Form::label('passing_year','Passing Year') !!}
									{!! Form::text('passing_year', $education->passing_year ,['placeholder'=>'2015','id'=>'datepicker2','class'=>'form-control']) !!}
									
								</div>									
								<div class="form-group">
							    	{!! Form::label('course_duration','Course duration(Years)') !!}
									{!! Form::text('course_duration', $education->course_duration ,['placeholder'=>'4','class'=>'form-control']) !!}
								</div>																	
							</div>
						</div>
						<div class="form-group">
						{!! Form::submit('Update',['class'=>'marg-top']) !!}
							
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
   		 </div>
  </div> 	
@endsection