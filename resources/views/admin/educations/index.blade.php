@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY EDUCATIONS</span>  || <a href="/educations/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
	 <div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
				<tr>
					<th colspan="12"><h2 class="text-center">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h4>My Educations</h4>	
							@endif
					</h2></th>
				</tr>				
				<tr>
					<th>Sl no</th>
					<th>Title</th>
					<th>Degree</th>
					<th>Institute</th>
					<th>Institute location</th>
					<th>Enrolled year</th>
					<th>Pasing year</th>
					<th>Result(GPA)</th>
					<th>Duration</th>
					<th>Board</th>
					<th colspan="2">Manage</th>
				</tr>
			</thead>
			<tbody>
			@php
				$sl=0;
			@endphp
			@forelse($educations as $education )
				@php
					$sl++;
				@endphp
				<tr>
					<td>{{ $sl }}</td>
					<td>{{ $education->title }}</td>
					<td>{{ $education->degree }}</td>
					<td>{{ $education->institute }}</td>
					<td>{{ $education->location }}</td>
					<td>{{ $education->enrolled_year }}</td>
					<td>{{ $education->passing_year }}</td>
					<td>{{ $education->result }}</td>
					<td>{{ $education->course_duration }}</td>
					<td>{{ (!empty($education->education_board))?$education->education_board:'NA' }}</td>
					<td>
						<a class="btn-success" href="/educations/{{ $education->id }}/edit">Edit</a> 
					</td>									
					<td>
						{{ Form::open(['url'=>['/educations',$education->id],'method'=>'DELETE']) }}
								{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
						{{ Form::close() }}
					</td>
			</tr>	

			@empty
				<h3>Educations not found</h3>
			@endforelse

													
			</tbody>
		</table>
	 </div>
 </div>
</div> 	
@endsection