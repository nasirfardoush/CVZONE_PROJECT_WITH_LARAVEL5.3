@extends('admin.layouts.master')
@section('facts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY FACTS</span>  || <a href="/facts/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
		<div class="table-responsive">
				<table class="table bg-slate-600">
					<thead>
						<tr>
						<th colspan="5">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h4>My Facts</h4>	
							@endif
						</th>
						</tr>				
						<tr>
							<th>Sl no.</th>
							<th>Facts Image</th>
							<th>Facts Title</th>
							<th>Facts Number</th>
							<th colspan="2">Manage</th>
						</tr>
					</thead>
					<tbody>
					 @php 
						$sl =0;
					 @endphp
					 @forelse($facts as $fact)
						@php 
							$sl++;
						@endphp
						<tr>
							<td>{{ $sl }}</td>
							<td> <img width="90px" height="70" src="{!! asset('images').'/'.$fact->img !!}" alt="No Image"> </td>
							<td>{{ $fact->title }}</td>
							<td>{{ $fact->no_of_items }}</td>
							<td>
								<a class="btn-success" href="/facts/{{ $fact->id }}/edit">Edit</a> ||

								{{ Form::open(['url'=>['/facts',$fact->id],'method'=>'DELETE']) }}
										{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
								{{ Form::close() }}
							</td>
						</tr>	
					 @empty
						<tr>
							<td colspan="5">
								<h3>Facts not found</h3>
							</td>
						</tr>
					 @endforelse
							
					</tbody>
				</table>
		</div>
	</div>
</div>		
@endsection