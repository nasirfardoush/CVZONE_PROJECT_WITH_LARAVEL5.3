@extends('admin.layouts.master')
@section('services_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY SERVICES</span>  || <a href="/services/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="9">
									@if(Session::has('message'))
										<div class="alert alert-info" >
											{{ Session::get('message') }}
										</div>
									@else
										<h4>My Services</h4>	
									@endif
                                    </th>	
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Feture image</th>
									<th>Title</th>
									<th>Description</th>
									<th>Integreted topics</th>
									<th>Clinte image</th>
									<th>Clinte feedback</th>
									<th colspan="3">Manage</th>
								</tr>
							</thead>
							<tbody>

							 @php 
								$sl =0;
					 		@endphp
					 		@forelse($services as $service)
								@php 
									$sl++;
								@endphp
								<tr>
										<td>{{ $sl }}</td>
										<td>
											<img width="90px" height="70" src="{!! asset('images').'/'.$service->img !!}" alt="No Image">
										</td>
										<td>{{ $service->title }}</td>
										<td>
											<p class="text-justify">{{ $service->description }}....</p>
										</td>
										<td>{{ $service->topics }}</td>
										<td>
											<img width="90px" height="70" src="{!! asset('images').'/'.$service->client_image !!}" alt="No Image">
										</td>
										<td>
											<p class="text-justify">{{ $service->clinte_feedback }}....</p>
										</td>
										<td>
											<a class="btn-success" href="/services/{{ $service->id }}">view</a> 
										</td>									
										<td>
											<a class="btn-success" href="/services/{{ $service->id }}/edit">Edit</a> 
										</td>									
										<td>
											{{ Form::open(['url'=>['/services',$service->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
											{{ Form::close() }}
										</td>
									</tr>
							@empty
								<tr>
									<td colspan="9">
										<h2>Service not found !</h2>
									</td>
								</tr>
							@endforelse	
									
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection