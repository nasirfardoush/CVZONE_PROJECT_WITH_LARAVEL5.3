@extends('admin.layouts.master')
@section('services_menu_add','active')
@section('pageTitle')
<span class="text-semibold">SERVICES - ADD</span>  || <a href="/services">MY SERVICES</a>
@endsection

@section('content')
	<div class="row ">
			 {!! Form::open(['url'=>'/services','method'=>'POST','files'=>'true']) !!}
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
								
								@endif
								<!-- section one -->
								<div class="col-md-5">
									<h5>Please add your services here .</h5>
									<div class="form-group">
										{!! Form::label('title','Facts Title') !!}
										{!! Form::text('title',null,['placeholder'=>'Design Services','class'=>'form-control']) !!}	
									</div>
									<div class="form-group">
										{!! Form::label('img','Servicess feture image') !!}
										{!! Form::file('img',['class'=>'form-control']) !!}	
										
									</div>												
									<div class="form-group">
										{!! Form::label('title','Description') !!}
										{!! Form::textarea('description',null,['class'=>'form-control']) !!}
									</div>										
									<div class="form-group">
										{!! Form::label('topics','Facts Title') !!}
										{!! Form::text('topics',null,['placeholder'=>'web design,Logodesign','class'=>'form-control ']) !!}	
										<small>Please topics devited by comma(,)</small>
									</div>									
								</div>
								<!-- Second section -->							
								<div class="col-md-5">
								<h6>Please write your client feedback here.</h6>
									<div class="form-group">
										{!! Form::label('client_image','Client image') !!}
										{!! Form::file('client_image',['class'=>'form-control']) !!}	
									</div>					
									<div class="form-group">
										{!! Form::label('clinte_feedback','Feedback') !!}
										{!! Form::text('clinte_feedback',null,['placeholder'=>'Good working','class'=>'form-control']) !!}
									</div>																
								</div>								
							</div>

							<div class="form-group">
							{!! Form::submit('Save',['class'=>'marg-top']) !!}
								
							</div>
						</div>
					</div>
				</fieldset>
			{!! Form::close() !!}	
   		 </div>	
@endsection