@extends('admin.layouts.master')
@section('services_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">SERVICES - EDIT</span>  || <a href="/services">MY SERVICES</a> || <a href="/services/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
			{!! Form::open(['url'=>['/services',$service->id],'method'=>'PUT','files'=>'true']) !!}
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-5">
									<h5>Edit your services  .</h5>
									<div class="form-group">
										{!! Form::label('title','Facts Title') !!}
										{!! Form::text('title',$service->title,['placeholder'=>'Design Services','class'=>'form-control']) !!}	
									</div>
																				
									<div class="form-group">
										{!! Form::label('title','Description') !!}
										{!! Form::textarea('description',$service->description,['class'=>'form-control']) !!}
									</div>										
									<div class="form-group">
										{!! Form::label('topics','Facts Title') !!}
										{!! Form::text('topics',$service->topics,['placeholder'=>'web design,Logodesign','class'=>'form-control ']) !!}	
										<small>Please topics devited by comma(,)</small>
									</div>	
									<div class="form-group">
										{!! Form::label('img','Servicess feture image') !!}
										{!! Form::file('img',['class'=>'form-control']) !!}	
										<img width="90px" height="70" src="{!! asset('images').'/'.$service->img !!}" alt="No Image">
									</div>									
								</div>
								<!-- Second section -->							
								<div class="col-md-5">
								<h6>Please write your client feedback here.</h6>

									<div class="form-group">
										{!! Form::label('clinte_feedback','Feedback') !!}
										{!! Form::text('clinte_feedback',$service->clinte_feedback,['placeholder'=>'Good working','class'=>'form-control']) !!}
									</div>	
									<div class="form-group">
										{!! Form::label('client_image','Client image') !!}
										{!! Form::file('client_image',['class'=>'form-control']) !!}	
										<img width="90px" height="70" src="{!! asset('images').'/'.$service->client_image !!}" alt="No Image">
									</div>																
								</div>								
							</div>

							<div class="form-group">
							{!! Form::submit('Save',['class'=>'marg-top']) !!}
								
							</div>
						</div>
					</div>
				</fieldset>
			{!! Form::close() !!}	
   		 </div>
  </div> 
@endsection