@extends('admin.layouts.master')
@section('awards_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY AWARDS</span>  || <a href="/awards/create">ADD NEW</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
		<div class="table-responsive">
				<table class="table bg-slate-600">
					<thead>
						<tr>
							<th colspan="8">
								@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h4>My Awards</h4>	
							@endif
							</th>			
						</tr>				
						<tr>
							<th>Sl no</th>
							<th>Title</th>
							<th>Description</th>
							<th>Organization</th>
							<th>Location</th>
							<th>Awards year</th>
							<th colspan="2">Manage</th>
						</tr>
					</thead>
					<tbody>
					 @php
						$sl=0;
					@endphp
					@forelse($awards as $award )
						@php
							$sl++;
						@endphp
						<tr>
							<td>{{ $sl }}</td>
							<td>{{ $award->title }}</td>
							<td>
								<p class="text-justify">{{ $award->description }}</p>
							</td>
							<td>{{ $award->organization }}</td>
							<td>{{ $award->location }}</td>
							<td>{{ $award->year }}</td>
							<td>
								<a class="btn-success" href="/awards/{{ $award->id }}/edit">Edit</a> 
							</td>									
							<td>
								{{ Form::open(['url'=>['/awards',$award->id],'method'=>'DELETE']) }}
								{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
						 {{ Form::close() }}
							</td>
						</tr>		
					@empty
						<tr>
							<td colspan="8" >
								<h2>Awards not found</h2>
							</td>
						</tr>
					@endforelse
							 	
					</tbody>
				</table>
		</div>
 </div>
</div>
@endsection