@extends('admin.layouts.master')
@section('posts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY POSTS</span>  || <a href="/posts/create">ADD NEW</a>
@endsection

@section('content')
<!-- Main content -->
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
			   							@if(Session::has('message'))
											<div class="alert alert-info" >
												{{ Session::get('message') }}
											</div>
										@else
											<h4>My Posts</h4>	
										@endif
			                                    
			                        </th>
								</tr>

								<tr>
									<th>Image</th>
									<th>Title</th>
									<th>Description</th>
									<th>Category name</th>
									<th>Tags</th>
									<th>Author name</th>
									<th>City</th>
									<th>Country</th>
									<th colspan="3">Manage</th>
								</tr>
							</thead>
							<tbody>
								
								 @forelse($posts as $post)
									<tr>
										<td><img  width="60" height="50" src="{!! asset('images').'/'.$post->img !!}" alt="Image" > </td>
										<td>{{ $post->title }}</td>
										<td >
											<p class="text-justify">{{ $post->description }}.....</p>
										</td>
										<td>{{ $post->categories }}</td>
										<td>{{ $post->tags }}</td>
										<td>{{ $post->author_name }}</td>
										<td>{{ $post->city_name }}</td>
										<td>{{ $post->country_name }}</td>
										<td>
	                                        <a class="btn-success" href="/posts/{{ $post->id }}">View</a>
										</td>
										<td>
	                                        <a class="btn-success" href="/posts/{{ $post->id }}/edit">Edit</a>
	                                      </td>  
										<td>
											{{ Form::open(['url'=>['/posts',$post->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
											{{ Form::close() }}
										</td>
									</tr>		
								@empty
								<tr>
									<td colspan="5">
										<h3>Posts not  found</h3>
									</td>
								</tr>
								@endforelse	
																


							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->
@endsection