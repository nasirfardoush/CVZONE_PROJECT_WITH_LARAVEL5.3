@extends('admin.layouts.master')
@section('posts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">POSTS - VIEW DETAILS</span> || <a href="/posts">MY POSTS</a> || <a href="/posts/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10"><h2 class="text-center">Post Details</h2></th>
								</tr>				
								<tr>
									<th>Image</th>
									<th>Title</th>
									<!-- <th>Description</th> -->
									<th>Category name</th>
									<th>Tags</th>
									<th>Author name</th>
									<th>City</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
									<img width="90" height="70" src="{!! asset('images').'/'.$post->img !!}" alt="No Image"> 
									</td>
									<td>{{ $post->title }}</td>
									<td>{{ $post->categories }}</td>
									<td>{{ $post->tags }}</td>
									<td>{{ $post->author_name }}</td>
									<td>{{ $post->city_name }}</td>
									<td>{{ $post->country_name }}</td>
								</tr>
								<tr>
									<td  colspan="7">
										<label>Post Description :</label>
										<p class="text-justify">{{ $post->description }}</p>
									</td>									
								</tr>
								<tr>
									<td colspan="7">									
										<a class="btn-success" href="/posts/{ {{ $post->id }} /edit">Edit</a> ||
										{{ Form::open(['url'=>['/posts',$post->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
											{{ Form::close() }}
									</td>
								</tr>															
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection