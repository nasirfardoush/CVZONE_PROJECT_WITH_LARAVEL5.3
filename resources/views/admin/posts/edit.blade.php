@extends('admin.layouts.master')
@section('posts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">POSTS - EDIT</span> || <a href="/posts">MY POSTS</a> || <a href="/posts/create">ADD NEW</a>
@endsection

@section('content')
    <div class="row ">
        <!-- about basic info about module -->
             {!! Form::open(['url'=>['/posts',$post->id],'method'=>'PUT','files'=>'true']) !!}
                <fieldset class="content-group">
                    <div class="form-group">
                        <div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">

                                        {!! Form::label('title','Posts Title') !!}
                                        {!! Form::text('title',$post->title ,['placeholder'=>'Learn PHP','class'=>'form-control']) !!}
                                        
                                    </div>  
                                    <div class="form-group">
                                        {!! Form::label('author_name','Author name') !!}
                                        {!! Form::text('author_name',$post->author_name ,['placeholder'=>'Rahim','class'=>'form-control']) !!}
                                        
                                    </div>                                              
                                    <div class="form-group">
                                        {!! Form::label('categories','Categoty name') !!}
                                        {!! Form::text('categories',$post->categories ,['placeholder'=>'Education','class'=>'form-control']) !!}
                                        
                                    </div>                                      
                                    
                                    <div class="form-group">
                                        {!! Form::label('description','Description') !!}
                                        {!! Form::textarea('description',$post->description ,['class'=>'form-control']) !!}
                                        
                                    </div>                                  
                                </div>                              
                                <!-- Second section -->                         
                                <div class="col-md-5">
                                    <div class="form-group">
                                        {!! Form::label('country_name','Country Name') !!}
                                        {!! Form::text('country_name',$post->country_name ,['placeholder'=>'Bangladesh','class'=>'form-control']) !!}
                                    </div>                  
                                    <div class="form-group">
                                        {!! Form::label('city_name','City Name') !!}
                                        {!! Form::text('city_name',$post->city_name ,['placeholder'=>'Dhaka','class'=>'form-control']) !!}
                                    </div>                                      
                                    <div class="form-group">
                                        {!! Form::label('tags','Tags') !!}
                                        {!! Form::text('tags',$post->tags ,['placeholder'=>'PHP','class'=>'form-control']) !!}
                                    </div>                                      
                                    <div class="form-group">
                                        {!! Form::label('img','Post Image') !!}
                                        {!! Form::file('img',['class'=>'form-control']) !!}
                                        <img width="90px" height="70" src="{!! asset('images').'/'.$post->img !!}" alt="No Image">
                                    </div>                  
                                </div>
                            </div>
                            
                            <div class="form-group">
                                {!! Form::submit('Update') !!}
                            </div>
                        </div>
                    </div>
                </fieldset>
            {!! Form::close() !!}
    </div>
@endsection