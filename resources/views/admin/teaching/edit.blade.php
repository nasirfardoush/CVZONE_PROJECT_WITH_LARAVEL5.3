@extends('admin.layouts.master')
@section('teaching_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">TEACHING - EDIT</span>  || <a href="/teaching">MY TEACHING</a> || <a href="/teaching/create">ADD NEW</a>
@endsection

@section('content')
    <div class="row ">

        <!-- Teaching Module -->
            {!! Form::open(['url'=>['/teaching',$teach->id],'method'=>'PUT']) !!}
                <fieldset class="content-group">
                    <div class="form-group">
                        <div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
                            <div class="row">
                                @if(Session::has('message'))
                                <div class="alert alert-info " >
                                    {{ Session::get('message') }}
                                </div>
                                @else
                                    <h5>Add your teaching .</h5>
                                @endif
                                <!-- section one -->
                                <div class="col-md-5">
                                    <div class="form-group">
                                        {!! Form::label('title','Teaching Title') !!}
                                        {!! Form::text('title', $teach->title ,['placeholder'=>'Professor','class'=>'form-control']) !!}   
                                    </div>              
                                    <div class="form-group">
                                        {!! Form::label('start_date','Start Year') !!}
                                        {!! Form::text('start_date', $teach->start_date ,['placeholder'=>'2001','id'=>'datepicker','class'=>'form-control']) !!}    
                                        
                                    </div>                                      
                                    <div class="form-group">
                                        {!! Form::label('teaching_desc','Sort description') !!}
                                        {!! Form::textarea('teaching_desc', $teach->teaching_desc ,['class'=>'form-control']) !!}
                                    </div>                                  
                                </div>                              
                                <!-- Second section -->                         
                                <div class="col-md-5">                      
                                    <div class="form-group">
                                        {!! Form::label('institute','Institute Name') !!}
                                        {!! Form::text('institute', $teach->institute ,['placeholder'=>'Daka university','class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('end_date','End Year') !!}
                                        {!! Form::text('end_date', $teach->end_year ,['placeholder'=>'2001','id'=>'datepicker2','class'=>'form-control']) !!} 
                                    </div>                                  
                                    <div class="form-group">
                                    {!! Form::label('teaching_status','Status') !!}

                                    {!! Form::select('teaching_status',
                                                 [
                                                    $teach->teaching_status => $teach->teaching_status, 
                                                    'CURRENT'  => 'Curent', 
                                                    'PREVIOUS' => 'Privious'
                                                 ],
                                         null,['class' => 'form-control']) !!}  
                                    
                                    </div>                      
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Update',['class'=>'marg-top']) !!}
                            </div>
                        </div>
                    </div>
                </fieldset>
            {{Form::close()}}
    </div>
@endsection