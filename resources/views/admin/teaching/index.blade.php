@extends('admin.layouts.master')
@section('teaching_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY TEACHING</span>  || <a href="/teaching/create">ADD NEW</a>
@endsection
@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
										@if(Session::has('message'))
										<div class="alert alert-info" >
											{{ Session::get('message') }}
										</div>
									@else
										<h4>My Teaching</h4>	
									@endif
									</th>
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Institute</th>
									<th>Start year</th>
									<th>End year</th>
									<th>Description</th>
									<th>Status</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
							@php
								$sl=0;
							@endphp
							@forelse($teaching as $teach )
								@php
									$sl++;
								@endphp
									<tr>
										<td>{{ $sl }}</td>
										<td>{{ $teach->title }}</td>
										<td>{{ $teach->institute }}</td>
										<td>{{ $teach->start_date }}</td>
										<td>{{ $teach->end_date }}</td>
										<td>
											<p class="text-justify">{{ $teach->teaching_desc }}</p>
										</td>
										<td>{{ $teach->teaching_status }}</td>
										<td>
											<a class="btn-success" href="/teaching/{{ $teach->id }}/edit">Edit</a> 
										</td>									
										<td>
											{{ Form::open(['url'=>['/teaching',$teach->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
											{{ Form::close() }} 
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="10" >
											<h3>Teaching not found</h3>
										</td>
									</tr>
								@endforelse																
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection