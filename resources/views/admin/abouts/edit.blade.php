@extends('admin.layouts.master')
@section('abouts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">ABOUT EDIT</span>  | <a href="/abouts/{{ $about->id }}"> ABOUT ME</a>
@endsection

@section('content')
<!-- Main content -->	
<div class="row ">
    <!-- about basic info about module -->
		{!! Form::open(['url'=>['/abouts',$about->id],'method'=>'PUT']) !!}
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">							
							<div class="form-group">
								{!! Form::label('phone','Phone number') !!}
								{!! Form::text('phone',$about->phone,['placeholder'=>'017*****','class'=>'form-control']) !!}
							</div>					
							<div class="form-group">
								{!! Form::label('work_area','Skills work area') !!}
								{!! Form::text('work_area',$about->work_area,['placeholder'=>'Designer,Develop etc.','class'=>'form-control']) !!}
								<small>Add another work area and separeted with comma(,) .</small>
							</div>

							<div class="form-group">
								{!! Form::label('short_desc','Sort description about you') !!}
								{!! Form::text('short_desc',$about->short_desc,['placeholder'=>'Something about you..','class'=>'form-control']) !!}
							</div>									
							<div class="form-group">
								{!! Form::label('bio','Sort description about you') !!}
								{!! Form::textarea('bio',$about->bio,['placeholder'=>'About yourself..','class'=>'form-control']) !!}
								
							</div>
								
							<div class="form-group">
								{!! Form::submit('Update') !!}
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>
@endsection	 