@extends('admin.layouts.master')
@section('abouts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">ABOUT ME</span>  | <a href="/abouts/{{ $about->id }}/edit"> UPDATE</a>
@endsection

@section('content')
<!-- Main content -->

	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="5">
									@if(Session::has('message'))
										<div class="alert alert-info" >
											{{ Session::get('message') }}
										</div>
									@else
										<h4>About Me</h4>	
									@endif
									</th>
								</tr>				
								<tr>
									<th>Phone</th>
									<th>Skills tag</th>
									<th>Sort description</th>
									<th>BIO</th>
									<th >Manage</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<td>{{ $about->phone }}</td>
									<td>{{ $about->work_area }}</td>
									<td>{{ $about->short_desc }}</td>
									<td >
										<p class="text-justify">{{ $about->bio }}</p>
									</td>
									<td>
										<a class="btn-success" href="/abouts/{{ $about->id }}/edit">Edit</a> 
									</td>									
								</tr>
								

							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->
@endsection
