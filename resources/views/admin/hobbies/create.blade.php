@extends('admin.layouts.master')
@section('hobbies_menu_add','active')
@section('pageTitle')
<span class="text-semibold">HOBBIES - ADD</span>  | <a href="/hobbies"> MY HOBBIES</a>
@endsection

@section('content')
	<div class="row ">
		<!-- Add hobbies -->
		{!! Form::open(['url'=>'/hobbies','method'=>'POST', 'files'=>'true']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@endif
								<div class="form-group">
									{!! Form::label('title','Hobbies Title') !!}
									{!! Form::text('title',null,['placeholder'=>'Fishing','class'=>'form-control input-xlg']) !!}

								</div>
								<div class="form-group">

									{!! Form::label('img','Hobbies image') !!}
									{!! Form::file('img',['class'=>'form-control input-xlg']) !!}

								</div>					
								<div class="form-group">

									{!! Form::label('description','Hobbies description') !!}
									{!! Form::textarea('description',null,['class'=>'form-control']) !!}

								</div>
								<div class="form-group">
									{!! Form::submit('Save') !!}

									
								</div>
							</div>
							</div>
						</div>
				</div>
			</fieldset>
		{!! Form::close() !!}	 </div>
  </div>	
@endsection