@extends('admin.layouts.master')
@section('hobbies_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY HOBBIES</span>  | <a href="/hobbies/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row">
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
			<div class="table-responsive">

					<table class="table bg-slate-600">
						<thead>
							<tr>
								<th colspan="4">
									@if(Session::has('message'))
										<div class="alert alert-info" >
											{{ Session::get('message') }}
										</div>
									@else
										<h4>My Hobbies</h4>	
									@endif
								</th>
							</tr>				
							<tr>
								<th>Image</th>
								<th>Title</th>
								<th>Description</th>
								<th colspan="2">Manage</th>
							</tr>
						</thead>
						<tbody>
						@forelse($hobbies as $hobby)
							<tr>
								<td><img width="90" height="70" src="{{ asset('images').'/'.$hobby->img }}" alt="No Image"> 
								</td>
								<td>
									{{ $hobby->title }}
								</td>
								<td><p class="text-justify">
									{{ $hobby->description }}
								</p></td>
								<td>
									<a class="btn-success" href="/hobbies/{{ $hobby->id }}/edit">Edit</a>
								</td>
								<td>
									{{ Form::open(['url'=>['/hobbies',$hobby->id],'method'=>'DELETE']) }}
										{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
									{{ Form::close() }}
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="4">
									<h3>Hobbies Not found!</h3>
								</td>
							</tr>
						@endforelse
						</tbody>
					</table>
			</div>
	 </div>
</div>	
@endsection