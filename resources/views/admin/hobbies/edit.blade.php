@extends('admin.layouts.master')
@section('hobbies_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">HOBBIES - EDIT</span>  | <a href="/hobbies"> MY HOBBIES</a> | <a href="/hobbies/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row ">

	<!-- Edit hobbies -->
	{!! Form::open(['url'=>['/hobbies',$hobby->id],'method'=>'PUT', 'files'=>'true']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							<h5>Edit your hobbies</h5>
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@endif
								<div class="form-group">
									{!! Form::label('title','Hobbies Title') !!}

									{!! Form::text('title',$hobby->title ,['placeholder'=>'Fishing','class'=>'form-control input-xlg']) !!}

								</div>			
								<div class="form-group">

									{!! Form::label('description','Hobbies description') !!}

									{!! Form::textarea('description',$hobby->description,['class'=>'form-control']) !!}

								</div>
								<div class="form-group">

									{!! Form::label('img','Hobbies image') !!}

									{!! Form::file('img',['class'=>'form-control input-xlg']) !!}
									<img width="90" height="70" src="{{ asset('images').'/'.$hobby->img }}" alt="No Image">

								</div>		
								<div class="form-group">
									{!! Form::submit('Update') !!}

									
								</div>
							</div>
							</div>
						</div>
				</div>
			</fieldset>
		{!! Form::close() !!}	 </div>

	 </div>
 </div>
 @endsection	