@extends('admin.layouts.master')
@section('portfolios_menu_add','active')
@section('pageTitle')
<span class="text-semibold">PORTFOLIOS - ADD</span>  || <a href="/portfolios">MY PORTFOLIOS</a>
@endsection

@section('content')
<div class="row">
	 {!! Form::open(['url'=>'/portfolios','method'=>'POST','files'=>'true']) !!}
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
						@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Please add your portfolios here .</h5>
							@endif
							<div class="form-group">
								{!! Form::label('title','Project Title') !!}
								{!! Form::text('title',null,['placeholder'=>'Ex. web design','class'=>'form-control']) !!}	
							</div>	
							<div class="form-group">
								{!! Form::label('img','Project image') !!}
								{!! Form::file('img',['class'=>'form-control']) !!}
							</div>														
							<div class="form-group">
								{!! Form::label('category','Category') !!}
								{!! Form::text('category',null,['placeholder'=>'Ex. Logo design','class'=>'form-control']) !!}								
							</div>								
							<div class="form-group">
									{!! Form::label('description','Project description') !!}
									{!! Form::textarea('description',null,['class'=>'form-control']) !!}

							</div>

							<div class="form-group">
								{!! Form::submit('Save') !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
 {!! Form::close() !!}
</div>
@endsection