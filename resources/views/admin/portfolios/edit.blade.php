@extends('admin.layouts.master')
@section('portfolios_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">PORTFOLIOS - EDIT</span>  || <a href="/portfolios">MY PORTFOLIOS</a> || <a href="/portfolios/create">ADD NEW</a>
@endsection

@section('content')
<div class="row" >
	 {!! Form::open(['url'=>['/portfolios',$portfolio->id],'method'=>'PUT','files'=>'true']) !!}
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
						@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Edit yours portfolio .</h5>
							@endif
							<div class="form-group">
								{!! Form::label('title','Project Title') !!}
								{!! Form::text('title', $portfolio->title ,['placeholder'=>'Ex. web design','class'=>'form-control']) !!}	
							</div>														
							<div class="form-group">
								{!! Form::label('category','Category') !!}
								{!! Form::text('category', $portfolio->category ,['placeholder'=>'Ex. Logo design','class'=>'form-control']) !!}								
							</div>								
							<div class="form-group">
									{!! Form::label('description','Project description') !!}
									{!! Form::textarea('description', $portfolio->description ,['class'=>'form-control']) !!}

							</div>
							<div class="form-group">
								{!! Form::label('img','Project image') !!}
								{!! Form::file('img',['class'=>'form-control']) !!}
								<img width="90px" height="70" src="{!! asset('images').'/'.$portfolio->img !!}" alt="No Image">
							</div>	

							<div class="form-group">
								{!! Form::submit('Update') !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
 {!! Form::close() !!}
</div>		
@endsection