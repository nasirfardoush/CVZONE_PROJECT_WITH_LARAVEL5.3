@extends('admin.layouts.master')
@section('portfolios_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY PORTFOLIOS</span>  || <a href="/portfolios/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="7">
									@if(Session::has('message'))
										<div class="alert alert-info" >
											{{ Session::get('message') }}
										</div>
									@else
										<h4>My Portfolios</h4>	
									@endif
									
									</th>
								</tr>				
								<tr>
									<th>Sl no.</th>
									<th>Project Image</th>
									<th>Project Title</th>
									<th>Ctegory</th>
									<th>Description</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
							@php
								$sl=0;
							@endphp
							@forelse($portfolios as $portfolio )
								@php
									$sl++;
								@endphp	
									<tr>
										<td>{{ $sl }}</td>
										<td> 
											<img width="90px" height="70" src="{!! asset('images').'/'.$portfolio->img !!}" alt="No Image"> 
										</td>
										<td>{{ $portfolio->title }}</td>
										<td>{{ $portfolio->category }}</td>
										<td>
											<p class="text-justify">{{ $portfolio->description }}...</p>
										</td>
										<td>
											<a class="btn-success" href="/portfolios/{{ $portfolio->id }}">View</a>
										</td>										
										<td>
											<a class="btn-success" href="/portfolios/{{ $portfolio->id }}/edit">Edit</a>
										</td>
										<td>
											{{ Form::open(['url'=>['/portfolios',$portfolio->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
											{{ Form::close() }}
										</td>
									</tr>								
								@empty
									<tr>
										<td colspan="7">
											<h3>Portfolios not founds</h3>
										</td>
									</tr>
								@endforelse
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection