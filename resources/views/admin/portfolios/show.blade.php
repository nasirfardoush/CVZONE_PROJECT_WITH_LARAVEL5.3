@extends('admin.layouts.master')
@section('portfolios_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">PORTFOLIOS - VIEW DETAILS</span>  || <a href="/portfolios">MY PORTFOLIOS</a> || <a href="/portfolios/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10"><h2 class="text-center">Service events</h2></th>
								</tr>				
								<tr>
									<th>Project image</th>
									<th>Project Title</th>
									<th>Category</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
									<img width="90px" height="70" src="{!! asset('images').'/'.$portfolio->img !!}" alt="No Image">
									</td>
									<td>{{ $portfolio->title }}</td>
									<td>{{ $portfolio->category }}</td>
								</tr>
								<tr>
									<td  colspan="3">
										<label>Project description :</label>
										<p class="text-justify">{{ $portfolio->description }}</p>
									</td>									
								</tr>
								<tr>
									<td colspan="3">									
										<a class="btn-success" href="/portfolios/{{ $portfolio->id }}/edit">Edit</a> ||
										{{ Form::open(['url'=>['/portfolios',$portfolio->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
										{{ Form::close() }} 
									</td>
								</tr>															
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection