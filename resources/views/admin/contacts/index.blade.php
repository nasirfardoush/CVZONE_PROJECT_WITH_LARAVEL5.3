@extends('admin.layouts.master')
@section('contacts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY INBOX</span>
@endsection
@section('content')
<div class="panel panel-default">
	<div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
		    	<tr>
		    		<th>Sl.</th>
		    		<th>Name </th>
		    		<th>Email</th>
		    		<th>Message</th>
		    		<th>Action</th>
		    	</tr>	
		    	</thead>
		    	<tbody>
			    		
			    		<tr class="" >
			    			<td>01</td>
			    			<td>Your name</td>
			    			<td>Example@gmail.com</td>
			    			<td>
			    				<p class="text-justify" > Message body</p>
			    			</td>
			    		<td>
			    			<a href="show">View</a>||
			    			<a href="reply">Reply</a>||
			    			<a href="trash">Delete</a>
			    		</td>
			    	</tr>
		    	</tbody>
			</table>
		</div>
	</div>		 
@endsection