@extends('admin.layouts.master')
@section('skills_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">SKILLS - EDIT</span>  || <a href="/skills">MY SKILLS</a> || <a href="/skills/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
  
		    <!-- Teaching Module -->
			{!! Form::open(['url'=>['/skills',$skill->id],'method'=>'PUT']) !!}
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
								@else
									<h5>Edit your Skills .</h5>
								@endif
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										{!! Form::label('title','Title') !!}
										{!! Form::text('title', $skill->title ,['placeholder'=>'Programming','class'=>'form-control']) !!}
									</div>	
									<div class="form-group">
										{!! Form::label('experience_area','Skills area') !!}
										{!! Form::textarea('experience_area', $skill->experience_area ,['placeholder'=>'PHP,AJAX,HTML','class'=>'form-control']) !!}
										<small>Please separet your skills with comma(,)</small>
									</div>									
									<div class="form-group">
										{!! Form::label('description','Sort description') !!}
										{!! Form::textarea('description', $skill->description ,['placeholder'=>'Some about of skills','class'=>'form-control']) !!}
									</div>
								</div>																		
									<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										{!! Form::label('experience','Expreince(Years)') !!}
										{!! Form::text('experience', $skill->experience ,['placeholder'=>'3','class'=>'form-control']) !!}
									</div>						
								     <div class="form-group">

								   		{!! Form::label('level','Skills level') !!}
										{!! Form::select('level',
												 [
												 	$skill->level=>$skill->level,
												 	'Intermediate' => 'Intermediate', 
												 	'Experts' => 'Experts',
												 	'Advance' => 'Advance',
												 	'Master' => 'Master',
												 ],
										    null,['class' => 'form-control'])
										!!}
								     	</div>										
								     	<div class="form-group">
								     		{{ Form::label('category','Skills category') }}
								     		{{ Form::select('category',
								     			[
								     				$skill->category=>$skill->category,
								     				'1'=>'1',
								     				'2'=>'2',
								     				'3'=>'3',
								     				'4'=>'4',
								     			],
										    null,['class' => 'form-control']
								     		) }}
											
								     	</div>						
								</div>
							</div>
							<div class="form-group">
									{!! Form::submit('Update',['class'=>'marg-top']) !!}
							</div>
						</div>
					</div>	
				</fieldset>
			</form>	
   		 </div>
  </div> 
@endsection