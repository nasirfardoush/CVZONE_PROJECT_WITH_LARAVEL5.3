@extends('admin.layouts.master')
@section('skills_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY SKILLS</span>  || <a href="/skills/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
										@if(Session::has('message'))
										<div class="alert alert-info" >
											{{ Session::get('message') }}
										</div>
										@else
											<h4>My Skills</h4>	
										@endif
								  </th>
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Skill area</th>
									<th>Description</th>
									<th>Experience</th>
									<th>Level</th>
									<th>Category</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
								@php
									$sl=0;
								@endphp
								@forelse($skills as $skill )
									@php
										$sl++;
									@endphp
									<tr>
										<td>{{ $sl }}</td>
										<td>{{ $skill->title }}</td>
										<td>{{ $skill->experience_area }}</td>
										<td>
											<p class="text-justify">{{ $skill->description }}</p>
										</td>
										<td>{{ $skill->experience }}</td>
										<td>{{ $skill->level }}</td>
										<td>{{ $skill->category }} </td>
										<td>
											<a class="btn-success" href="/skills/{{ $skill->id }}/edit">Edit</a> 
										</td>									
										<td>
											{{ Form::open(['url'=>['/skills',$skill->id],'method'=>'DELETE']) }}
												{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
											{{ Form::close() }} 
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="10" >
											<h3>Skills not found</h3>
										</td>
									</tr>
								@endforelse							 															
							</tbody>
						</table>
				</div>
		 </div>
	</div>		
@endsection