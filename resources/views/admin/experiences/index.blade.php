@extends('admin.layouts.master')
@section('experiences_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY EXPERIENCE</span>  || <a href="/experiences/create">ADD NEW</a>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
	<div class="table-responsive">
			<table class="table bg-slate-600">
				<thead>
					<tr>
						<th colspan="9">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h4>My Expreinces</h4>	
							@endif
						</th>					
					</tr>				
					<tr>
						<th>Sl no</th>
						<th>Title</th>
						<th>Description</th>
						<th>Company</th>
						<th>Company location</th>
						<th>Start  year</th>
						<th>End year</th>
						<th colspan="2">Manage</th>
					</tr>
				</thead>
				<tbody>
					@php
						$sl=0;
					@endphp
					@forelse($experiences as $experience )
						@php
							$sl++;
						@endphp
					<tr>
						<td>{{ $sl }}</td>
						<td>{{ $experience->designation }}</td>
						<td>{{ $experience->expreince_desc }}</td>
						<td>{{ $experience->company_name }}</td>
						<td>{{ $experience->company_location }}</td>
						<td>{{ $experience->start_date }}</td>
						<td>{{ $experience->end_date }}</td>
						<td>
							<a class="btn-success" href="/experiences/{{ $experience->id }}/edit">Edit</a> 
						</td>									
						<td>
							{{ Form::open(['url'=>['/experiences',$experience->id],'method'=>'DELETE']) }}
								{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
						 {{ Form::close() }}
						</td>
					</tr>
					@empty
						<h3>Expreinces not found</h3>
					@endforelse							
				</tbody>
			</table>
	  </div>
   </div>
</div>	
@endsection