@extends('admin.layouts.master')
@section('experiences_menu_add','active')
@section('pageTitle')
<span class="text-semibold">EXPERIENCE - ADD</span>  || <a href="/experiences">MY EXPERIENCE</a>
@endsection

@section('content')
	<div class="row ">  
		    <!-- about basic info about module -->
			{!! Form::open(['url'=>'/experiences','method'=>'POST']) !!}
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
								@else
									<h5>Add your experiences .</h5>
								@endif							
								<!-- section one -->
									<div class="col-md-5">
										<div class="form-group">
											{!! Form::label('designation','Designation') !!}
											{!! Form::text('designation','',['placeholder'=>'PHP Developer','class'=>'form-control']) !!}
										</div>					
										<div class="form-group">
										    {!! Form::label('company_name','Company Name') !!}
											{!! Form::text('company_name','',['placeholder'=>'Webtech','class'=>'form-control']) !!}
										</div>
										<div class="form-group">
										  {!! Form::label('company_location','Company Location') !!}
										  {!! Form::text('company_location',null,['placeholder'=>'Bangladesh','class'=>'form-control']) !!}											
										</div>
										<div class="form-group">
										  {!! Form::label('expreince_desc','Sort description') !!}
										  {!! Form::textarea('expreince_desc',null,['class'=>'form-control']) !!}	
										</div>									
									</div>
									<!-- Section Two -->									
									<div class="col-md-5">				
										<div class="form-group">
										 {!! Form::label('start_date','Start Year') !!}
										 {!! Form::text('start_date',null,['placeholder'=>'2001','id'=>'datepicker','class'=>'form-control']) !!}	
										</div>	
										<div class="form-group">
										 {!! Form::label('end_date','End Year') !!}
										 {!! Form::text('end_date',null,['placeholder'=>'2007','id'=>'datepicker2','class'=>'form-control']) !!}	
								     	</div>																									
								 </div>	
							</div>
							<div class="form-group ">
							 {!! Form::submit('Save',['class'=>'marg-top']) !!}
							
							</div>	
						</div>
					</div>
				</fieldset>
			{!! Form::close() !!}
   		 </div>
  </div> 
@endsection