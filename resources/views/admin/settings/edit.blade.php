@extends('admin.layouts.master')
@section('settings_menu_manage','active')
 


@section('pageTitle')
<span class="text-semibold">SETTINGS - EDIT</span>
@endsection

@section('content')
<div class="row">
	<!--Setting options code-->
	{!! Form::open(['url'=>['/settings',$setting->id],'method'=>'PUT','files'=>'true']) !!}
		<fieldset class="content-group">
		<div class="form-group">
			<div class="col-lg-10">
				<div class="row">
					<div class="col-md-6 col-md-offset-1">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
										
							@endif
						
						<div class="form-group">
							{!! Form::label('featured_img','Profile Picture') !!}
							{!! Form::file('featured_img',['class'=>'form-control']) !!}	
						</div>					
						<div class="form-group">
							{!! Form::label('title','My Title') !!}
							{!! Form::text('title',$setting->title,['placeholder'=>'PHP Developer','class'=>'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('fullname','My name') !!}
							{!! Form::text('fullname',$setting->fullname,['placeholder'=>'Md Ryhan..','class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('address','Curent Location') !!}
							{!! Form::text('address',$setting->address,['placeholder'=>'london.','class'=>'form-control']) !!}
						</div>	
						<div class="form-group">
							<h3>Chose your Theme color</h3>
							{!! Form::radio('themecolor','blue',$setting->themecolor=='blue'?true:false) !!}
							{!! Form::label('blue','Blue') !!}	

							{!! Form::radio('themecolor','brown',$setting->themecolor=='brown'?true:false) !!}
							{!! Form::label('brown','Brown') !!}	

							{!! Form::radio('themecolor','cyan',$setting->themecolor=='cyan'?true:false) !!}
							{!! Form::label('cyan','Cyan') !!}	

							{!! Form::radio('themecolor','light-green',$setting->themecolor=='light-green'?true:false) !!}
							{!! Form::label('light-green','Light-green') !!}	

							{!! Form::radio('themecolor','orange',$setting->themecolor=='orange'?true:false) !!}
							{!! Form::label('orange','Orange') !!}	

							{!! Form::radio('themecolor','green',$setting->themecolor=='green'?true:false) !!}
							{!! Form::label('green','Default') !!}
							
						</div>
						<div class="form-group">
							{!! Form::submit('Update') !!}
						</div>
					</div>				
					<div class="col-md-4 col-lg-4 ">
						<h1></h1>
						<img width="100" height="90" src="{{ asset('images').'/'.$setting->featured_img }}" alt="Profile Image!!!" >
					</div>	
				</div>
			</div>
		</fieldset>
	</form>
	<!--Setting options code-->
</div>	
@endsection