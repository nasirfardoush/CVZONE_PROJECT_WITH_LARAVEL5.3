<!DOCTYPE html>
<html>
<head>
	<title>CVzone/search</title>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/custom.css')}}">
</head>
<body>
	<div class="pagewraper">
		<div class="menu">

			<ul>
				<li>
					<a href="{{ url('/register') }}">Sign up</a>
				</li>
				<li>
					 <a href="{{ url('/login') }}">Sign in</a>
				</li>
			</ul>
		</div>


		<div class="maincontent">
			<h1 class="logo">CV<span>zone</span></h1>

			<form action="search.php" method="_GET" >

				<div class="control">
					<input class="primaryInput" type="search" name="keyword">
					<select class="advanceInput"  name="searchByselect">
						<option  name="advanceSearch">Click on me for Search by category</option>
						<option value="skills">Skills</option>
						<option value="username">Username</option>
						<option value="services">Services</option>
						<option value="experiences">Experience</option>
						<option value="settings">Location</option>
					</select>
				</div>
				<p>
				<input class="submitbtn" type="submit" value="CVzone search" name="searchUser">
				</p>

			</form>

		</div>
	</div>
</body>
</html>