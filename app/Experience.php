<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Experience extends Model
{
	use SoftDeletes;
    protected $fillable = [
    'user_id','designation','company_name','company_location','expreince_desc','start_date','end_date',
        ];

     protected $dates = [ ' deleted_at'];
}
