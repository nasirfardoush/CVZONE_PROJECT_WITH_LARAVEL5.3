<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Service;
use Auth;
use Redirect;
use Session;

class ServicesController extends Controller
{
    
    public function index()
    {
        $services = Service::where('user_id','=',Auth::user()->id)->get();
        return view('admin.services.index',compact('services'));
    }


    public function create()
    {
         return view('admin.services.create');
        
    }


    public function store(Request $request)
    {
         if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;
         }         
          if (Input::hasFile('client_image')){
            $file = Input::file('client_image');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->client_image = $img_name;
         }

        Service::create([
            'user_id'       =>Auth::user()->id,
            'title'         =>$request->title,
            'img'           =>$request->img,
            'description'   =>$request->description,
            'topics'        =>$request->topics,
            'client_image'  =>$request->client_image,
            'clinte_feedback'=>$request->clinte_feedback,

        ]);

        Session::flash('message', 'Successfully Added');
         return Redirect::back();
    }


    public function show($id)
    {
        $service = Service::find($id);
        return view('admin.services.show',compact('service'));
    }


    public function edit($id)
    {
         $service = Service::find($id);
        return view('admin.services.edit',compact('service'));
    }


    public function update(Request $request, $id)
    {
            $services = Service::find($id);
            $services->title    =$request->title;

             if (Input::hasFile('img')){
                $file = Input::file('img');
                $img_name = time()."-".$file->getClientOriginalName();
                $file->move('images',$img_name);
                $request->img  = $img_name;
                $services->img =$request->img;
              }

            $services->description   =$request->description;
            $services->topics        =$request->topics;
             if (Input::hasFile('client_image')){
                $file = Input::file('client_image');
                $img_name = time()."-".$file->getClientOriginalName();
                $file->move('images',$img_name);
                $request->client_image    = $img_name;
                $services->client_image   =$request->client_image     ;
              }

            $services->clinte_feedback=$request->clinte_feedback;
            $services->save();

            Session::flash('message', 'Successfully  Updated');
            return Redirect::to('/services');

    }


    public function destroy($id)
    {
        $services = Service::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/services');
    }
}
