<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Award;
use Auth;
use Redirect;
use Session;
class AwardsController extends Controller
{

    public function index()
    {
        $awards = Award::where('user_id','=',Auth::user()->id)->get();
        return view('admin.awards.index',compact('awards'));
    }

  
    public function create()
    {
        return view('admin.awards.create');
    }


    public function store(Request $request)
    {
        Award::create([
            'user_id'       =>Auth::user()->id,
            'title'         =>$request->title,
            'year'          =>$request->year,
            'description'   =>$request->description,
            'organization'  =>$request->organization,
            'location'      =>$request->location,
        ]);
        Session::flash('message', 'Successfully Added');
         return Redirect::back();
    }

  
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $award = Award::find($id);
        return view('admin.awards.edit',compact('award'));
    }


    public function update(Request $request, $id)
    {
            $award = Award::find($id);
            $award->title         =$request->title;
            $award->year          =$request->year;
            $award->description   =$request->description;
            $award->organization  =$request->organization;
            $award->location      =$request->location;
            $award->save();

         Session::flash('message', 'Successfully Updated');
           return Redirect::to('/awards');
    }


    public function destroy($id)
    {
      $education = Award::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/awards');
    }
}
