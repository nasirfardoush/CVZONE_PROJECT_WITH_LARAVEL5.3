<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Redirect;
use App\Skill;
class SkillsController extends Controller
{
    
    public function index()
    {
        $skills = Skill::where('user_id','=',Auth::user()->id)->get();
        return view('admin.skills.index',compact('skills'));
    }

  
    public function create()
    {
         return view('admin.skills.create');
    }

    
    public function store(Request $request)
    {
        Skill::create([
        'user_id'           =>Auth::user()->id,
        'title'           => $request->title,
        'experience_area' => $request->experience_area,
        'description'     => $request->description,
        'experience'      => $request->experience,
        'level'           => $request->level,
        'category'        => $request->category,
        ]);

        Session::flash('message', 'Successfully Added');
         return Redirect::back();


    }

    
   
    public function edit($id)
    {
         $skill = Skill::find($id);
       return view('admin.skills.edit',compact('skill'));
    }

    
    public function update(Request $request, $id)
    {
        $skill = Skill::find($id);
        $skill->title           = $request->title;
        $skill->experience_area = $request->experience_area;
        $skill->description     = $request->description;
        $skill->experience      = $request->experience;
        $skill->level           = $request->level;
        $skill->category        = $request->category;
        $skill->save();
        
        Session::flash('message', 'Successfully Updated');
      return Redirect::to('/skills');

    }

    
    public function destroy($id)
    {
        $skills = Skill::destroy($id);
        Session::flash('message', 'Successfully deleted');
       return Redirect::to('/skills');
    }
}
