<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Experience;
use Auth;
use Redirect;
use Session;
class ExperiencesController extends Controller
{
 
    public function index()
    {
       $experiences = Experience::where('user_id','=',Auth::user()->id)->get();
        return view('admin.experiences.index',compact('experiences'));
    }

    
    public function create()
    {
        return view('admin.experiences.create');
    }

    
    public function store(Request $request)
    {
       Experience::create([
            'user_id'         =>Auth::user()->id,
            'designation'     =>$request->designation,
            'company_name'    =>$request->company_name,
            'company_location'=>$request->company_location,
            'expreince_desc'  =>$request->expreince_desc,
            'start_date'      =>$request->start_date,
            'end_date'        =>$request->end_date,
        ]);

         Session::flash('message', 'Successfully Added');
         return Redirect::back();
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {   
         $experience = Experience::find($id);
         return view('admin.experiences.edit',compact('experience'));
    }

    
    public function update(Request $request, $id)
    {
         $experience = Experience::find($id);
            $experience->designation     =$request->designation;
            $experience->company_name    =$request->company_name;
            $experience->company_location=$request->company_location;
            $experience->expreince_desc  =$request->expreince_desc;
            $experience->start_date      =$request->start_date;
            $experience->end_date        =$request->end_date;
            $experience->save();

            Session::flash('message', 'Successfully Updated');
           return Redirect::to('/experiences');
    }

    
    public function destroy($id)
    {
        $experience = Experience::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/experiences');
    }
}
