<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Post;
use Auth;
use Redirect;
use Session;

class PostsController extends Controller
{
    
    public function index()
    {
        
        $posts = Post::where('user_id','=',Auth::user()->id)->get();
        return view('admin.posts.index',compact('posts'));
    }

   
    public function create()
    {
       
        return view('admin.posts.create');
    }

    
    public function store(Request $request)
    {  
      if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;

            Post::create([
                'user_id'       =>Auth::user()->id,
                'title'         =>$request->title,
                'author_name'   =>$request->author_name,
                'categories'    =>$request->categories,
                'description'   =>$request->description,
                'country_name'  =>$request->country_name,
                'city_name'     =>$request->city_name,
                'tags'          =>$request->tags,
                'img'           =>$request->img,
            ]);

            Session::flash('message', 'Successfully Added');
            return Redirect::back();

        }else{
             Post::create([
                'user_id'       =>Auth::user()->id,
                'title'         =>$request->title,
                'author_name'   =>$request->author_name,
                'categories'    =>$request->categories,
                'description'   =>$request->description,
                'country_name'  =>$request->country_name,
                'city_name'     =>$request->city_name,
                'tags'          =>$request->tags,

            ]);
            
            Session::flash('message', 'Successfully Added with out image');
            return Redirect::back();
        }    
    }

  
    public function show($id)
    {
        $post =  Post::find($id);
        return view('admin.posts.show',compact('post'));
    }

    
    public function edit($id)
    {
        $post =  Post::find($id);
        return view('admin.posts.edit',compact('post'));
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
       if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;

                $post->title         =$request->title;
                $post->author_name   =$request->author_name;
                $post->categories    =$request->categories;
                $post->description   =$request->description;
                $post->country_name  =$request->country_name;
                $post->city_name     =$request->city_name;
                $post->tags          =$request->tags;
                $post->img           =$request->img;
                $post->save();
            

           Session::flash('message', 'Successfully Updated');
            return Redirect::to('/posts');

        }else{
                $post->title         =$request->title;
                $post->author_name   =$request->author_name;
                $post->categories    =$request->categories;
                $post->description   =$request->description;
                $post->country_name  =$request->country_name;
                $post->city_name     =$request->city_name;
                $post->tags          =$request->tags;
                $post->save();
            
            Session::flash('message', 'Successfully  Updated');
            return Redirect::to('/posts');
        }    
    }


   
    public function destroy($id)
    {
       $post = Post::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/posts');
    }
}
