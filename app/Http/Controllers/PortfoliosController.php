<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Portfolio;
use Session;
use Redirect;
use Auth;


class PortfoliosController extends Controller
{

    public function index()
    {  
        $portfolios = Portfolio::where('user_id','=',Auth::user()->id)->get();
        return view('admin.portfolios.index',compact('portfolios'));
    }

   
    public function create()
    {
        return view('admin.portfolios.create');
    }

    
    public function store(Request $request)
    {   
        if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;
         }   
        Portfolio::create([
            'user_id'   =>Auth::user()->id,
            'title'     =>$request->title,
            'img'       =>$request->img,
            'category'  =>$request->category,
            'description' =>$request->description,

         ]);

         Session::flash('message', 'Successfully Added');
         return Redirect::back();
    }

    
    public function show($id)
    {
         $portfolio = Portfolio::find($id);
        return view('admin.portfolios.show',compact('portfolio'));
    }

    
    public function edit($id)
    {
        $portfolio = Portfolio::find($id);
        return view('admin.portfolios.edit',compact('portfolio'));
    }

    
    public function update(Request $request, $id)
    {       
            $portfolio = Portfolio::find($id);
            
            if (Input::hasFile('img')){
                $file = Input::file('img');
                $img_name = time()."-".$file->getClientOriginalName();
                $file->move('images',$img_name);
                $request->img = $img_name;
                $portfolio->img       = $request->img;
             }   
                $portfolio->title     = $request->title;
                $portfolio->category  = $request->category;
                $portfolio->description = $request->description;
                $portfolio->save();

         Session::flash('message', 'Successfully Updated');
         return Redirect::to('/portfolios');
    }

    public function destroy($id)
    {
        $portfolio = Portfolio::destroy($id);
        Session::flash('message', 'Successfully Deleted');
         return Redirect::to('/portfolios');
    }
}
