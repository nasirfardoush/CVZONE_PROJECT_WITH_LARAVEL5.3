<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use Auth;
use Redirect;
use Session;
class AboutsController extends Controller
{
   
 


    public function show($id)
    {   $user_id = Auth::user()->id;

        $about = About::where('user_id','=',$user_id)->first();
        return view('admin.abouts.show',compact('about'));
    }

    
    public function edit($id)
    {
       $user_id = Auth::user()->id;

        $about = About::where('user_id','=',$user_id)->first();
        return view('admin.abouts.edit',compact('about'));
    }


    
    public function update(Request $request, $id)
    {
         $about = About::find($id);

         $about->phone = $request->phone;
         $about->work_area = $request->work_area;
         $about->short_desc = $request->short_desc;
         $about->bio = $request->bio;
         $about->save();

        Session::flash('message', 'Successfully Updated');
        $redirect = "/abouts/".$about->id;
        return Redirect::to($redirect);
    }

}
