<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\User;
use App\Setting;
use App\About;
use App\Award;
use App\Education;
use App\Experience;
use App\Hobbie;
use App\Fact;
use App\Portfolio;
use App\Post;
use App\Service;
use App\Skill;
use App\Teaching;
class PdfCntroller extends Controller
{

	function generate_pdf($username) {

        $allData['user'] = User::where('username','=',$username)->first();
        $user_id = $allData['user']->id;
        
        $allData['settings']    = Setting::where('user_id','=',$user_id )->first(); 
        $allData['about']       = About::where('user_id','=',$user_id )->first(); 
        $allData['awards']      = Award::where('user_id','=',$user_id )->get(); 
        $allData['educations']  = Education::where('user_id','=',$user_id )->get(); 
        $allData['experiences'] = Experience::where('user_id','=',$user_id )->get(); 
        $allData['hobbies']     = Hobbie::where('user_id','=',$user_id )->get(); 
        $allData['facts']       = Fact::where('user_id','=',$user_id )->get(); 
        $allData['portfolios']  = Portfolio::where('user_id','=',$user_id )->get(); 
        $allData['posts']       = Post::where('user_id','=',$user_id )->get(); 
        $allData['services']    = Service::where('user_id','=',$user_id )->get(); 
        $allData['skills']      = Skill::where('user_id','=',$user_id )->get(); 
        $allData['teaching']    = Teaching::where('user_id','=',$user_id )->get(); 

	     $pdf = PDF::loadView('profile.cv', compact('allData'));
	    return $pdf->stream('cv.profile');

	    
	}

}
