<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use Redirect;
class SettingsController extends Controller
{
  
   
    public function edit($id)
    {   
       $user_id = Auth::user()->id;

        $setting = Setting::where('user_id','=',$user_id)->first();
        return view('admin.settings.edit',compact('setting'));
    }

  
    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);

         if (Input::hasFile('featured_img')){
            $file       = Input::file('featured_img');
            $img_name   = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->featured_img = $img_name;
            $setting->featured_img  = $request->featured_img;
           } 
            $setting->title         = $request->title;
            $setting->fullname      = $request->fullname;
            $setting->address       = $request->address;
            $setting->themecolor    = $request->themecolor;
            $setting->save();

        Session::flash('message', 'Successfully  Updated');
        return Redirect::back();
 }

 
}
