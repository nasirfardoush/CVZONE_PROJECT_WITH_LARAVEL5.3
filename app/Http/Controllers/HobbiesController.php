<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Hobbie;
use Auth;
use Redirect;
use Session;


class HobbiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $hobbies = Hobbie::where('user_id','=',Auth::user()->id)->get();
        return view('admin.hobbies.index',compact('hobbies'));
    }


    public function create()
    {
        return view('admin.hobbies.create');
    }

    public function store(Request $request)
    {   
        if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;

            Hobbie::create([
            'user_id'      =>Auth::user()->id,
            'title'        =>$request->title,
            'description'  =>$request->description,
            'img'          =>$request->img,

            ]);

            Session::flash('message', 'Successfully Added');
            return Redirect::back();

        }else{
             Hobbie::create([
            'user_id'      =>Auth::user()->id,
            'title'        =>$request->title,
            'description'  =>$request->description,
            ]);
             Session::flash('message', 'Successfully Added');
            return Redirect::back();
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $hobby = Hobbie::find($id);
        return view('admin.hobbies.edit',compact('hobby'));
        
    }


    public function update(Request $request, $id)
    {
        $hobby = Hobbie::find($id);

        if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;

            $hobby->title =  $request->title;
            $hobby->description = $request->description;
            $hobby->img = $request->img;
            $hobby->save();

           
            Session::flash('message', 'Successfully Updated');
            return Redirect::to('/hobbies');

        }else{

            $hobby->title =  $request->title;
            $hobby->description = $request->description;
            $hobby->save();
            Session::flash('message','Successfully Updated');
             return Redirect::to('/hobbies');
        }

    }


    public function destroy($id)
    {

         $hobby = Hobbie::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/hobbies');
    
  }
}
