<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teaching;
use Auth;
use Redirect;
use Session;
class TeachingController extends Controller
{

    public function index()
    {
        $teaching = Teaching::where('user_id','=',Auth::user()->id)->get();
        return view('admin.teaching.index',compact('teaching'));
    }

   
    public function create()
    {
        
        return view('admin.teaching.create');
    }

   
    public function store(Request $request)
    {   Teaching::create([
            'user_id'       =>Auth::user()->id,
            'title'         =>$request->title,
            'teaching_desc' =>$request->teaching_desc,
            'start_date'    =>$request->start_date,
            'end_date'      =>$request->end_date,
            'institute'     =>$request->institute,
            'teaching_status'=>$request->teaching_status,
         ]);

        Session::flash('message', 'Successfully Added');
         return Redirect::back();

    }

    
    public function edit($id)
    {
        $teach = Teaching::find($id);
        return view('admin.teaching.edit',compact('teach'));
    }

   
    public function update(Request $request, $id)
    {
          $teach = Teaching::find($id);
            $teach->title         =$request->title;
            $teach->teaching_desc =$request->teaching_desc;
            $teach->start_date    =$request->start_date;
            $teach->end_date      =$request->end_date;
            $teach->institute     =$request->institute;
            $teach->teaching_status =$request->teaching_status;
            $teach->save();

            Session::flash('message', 'Successfully Updated');
           return Redirect::to('/teaching');

    }

    
    public function destroy($id)
    {
        $Teaching = Teaching::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/teaching');
    }
}
