<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Education;
use Auth;
use Redirect;
use Session;

class EducationsController extends Controller
{

    public function index()
    {   $educations = Education::where('user_id','=',Auth::user()->id)->get();
        return view('admin.educations.index',compact('educations'));
    }


    public function create()
    {
        return view('admin.educations.create');
    }


    public function store(Request $request)
    {
        Education::create([
            'user_id'       =>Auth::user()->id,
            'title'         =>$request->title,
            'degree'        =>$request->degree,
            'institute'     =>$request->institute,
            'location'      =>$request->location,
            'enrolled_year' =>$request->enrolled_year,
            'passing_year'  =>$request->passing_year,
            'result'        =>$request->result,
            'education_board' =>$request->education_board,
            'course_duration' =>$request->course_duration,

        ]);

        Session::flash('message', 'Successfully Added');
         return Redirect::back();

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
       $education = Education::find($id);
       return view('admin.educations.edit',compact('education'));
    }


    public function update(Request $request, $id)
    {
            $educations = Education::find($id);

            $educations->title           = $request->title;
            $educations->degree          = $request->degree;
            $educations->institute       = $request->institute;
            $educations->location        = $request->location;
            $educations->enrolled_year   = $request->enrolled_year;
            $educations->passing_year    = $request->passing_year;
            $educations->result          = $request->result;
            $educations->education_board = $request->education_board;
            $educations->course_duration = $request->course_duration;
            $educations->save();

            Session::flash('message', 'Successfully Updated');
           return Redirect::to('/educations');

    }


    public function destroy($id)
    {
        $education = Education::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/educations');
    }
}
