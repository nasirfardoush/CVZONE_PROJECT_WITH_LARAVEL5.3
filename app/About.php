<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
   protected $fillable = ['user_id','phone','work_area','short_desc','bio',];
}
