<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Post extends Model
{
    use SoftDeletes;
     protected $fillable=[
      'user_id','title','author_name','categories','description','country_name','city_name','tags','img',
   ];
    protected $dates = [ ' deleted_at'];
}
