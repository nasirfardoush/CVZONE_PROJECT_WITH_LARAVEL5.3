<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Service extends Model
{
	use SoftDeletes;
    protected $fillable = [
    'user_id','title','img','description','topics','client_image','clinte_feedback'
    ];
     protected $dates = [ ' deleted_at'];
}
