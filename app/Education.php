<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Education extends Model
{
   protected $table = 'educations';
   use SoftDeletes;
   protected $fillable = [
   	'user_id','title','degree','institute','location','enrolled_year','passing_year','result','education_board','course_duration',
      ];
}
