<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Hobbie extends Model
{
		use SoftDeletes; 

       protected $fillable = [
       	'title','user_id','description','img',
       ];

     protected $dates = ['deleted_at'];
}
