<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Portfolio extends Model
{
	use SoftDeletes;
    protected $fillable = [
    	'user_id','title','img','category','description',
       ];
     protected $dates = ['created_at'];
}
