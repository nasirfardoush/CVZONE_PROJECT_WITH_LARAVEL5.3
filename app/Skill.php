<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Skill extends Model
{
	use SoftDeletes;
    protected $fillable = [
    	'user_id','title','experience_area','description','experience','level','category',  
      ];

      protected $dates = ['deleted_at'];
}
