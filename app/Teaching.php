<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Teaching extends Model
{	
	use SoftDeletes;
    protected $table = 'teaching';

    protected $fillable = [
		'user_id','title','teaching_desc','start_date','end_date','institute','teaching_status'
    ];

}
