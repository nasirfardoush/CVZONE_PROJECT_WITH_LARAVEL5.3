<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->word,
        'first_name' => $faker->word.'first',
        'last_name' => $faker->word.'last',
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('1234567'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Setting::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'user_id' => App\User::all()->random()->id,
        'themecolor'=>'green',
    ];
});
$factory->define(App\About::class, function (Faker\Generator $faker) {

    return [
        'user_id' => App\User::all()->random()->id,
       
    ];
});
