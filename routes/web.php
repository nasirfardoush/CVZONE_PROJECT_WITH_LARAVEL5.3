<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('profile/{username}', 'ProfileController@profile');
Route::get('profile/cv/{username}', 'PdfCntroller@generate_pdf');

Route::group(['middleware'=>'authenticated'],function(){

	Route::get('/admin',function(){
		return view('admin.users.dashboard');
	});
	Route::get('/home',function(){
		return view('admin.users.dashboard');
	});

	Route::resource('/hobbies','HobbiesController');
	Route::resource('/facts','FactsController');
	Route::resource('/educations','EducationsController');
	Route::resource('/experiences','ExperiencesController');
	Route::resource('/awards','AwardsController');
	Route::resource('/posts','PostsController');
	Route::resource('/services','ServicesController');
	Route::resource('/teaching','TeachingController');
	Route::resource('/skills','SkillsController');
	Route::resource('/portfolios','PortfoliosController');
	Route::resource('/settings','SettingsController');
	Route::resource('/abouts','AboutsController');
});